# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-04-26 19:59+0200\n"
"PO-Revision-Date: 2021-03-14 13:02+0000\n"
"Last-Translator: b'prenom_de_QTF nom_de_QTF <QTF@mail.com>'\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Translated-Using: django-rosetta 0.9.5\n"

#: FlOpEDT/settings/base.py:157
msgid "French"
msgstr "Français"

#: FlOpEDT/settings/base.py:158
msgid "English"
msgstr "Anglais"

#: FlOpEDT/settings/base.py:159
msgid "Spanish"
msgstr "Espagnol"

#: FlOpEDT/settings/base.py:160
msgid "Arabic"
msgstr "Arabe"

#: FlOpEDT/settings/base.py:161
msgid "Basque"
msgstr "Basque"

#: FlOpEDT/settings/base.py:162
msgid "Breton"
msgstr "Breton"

#: FlOpEDT/settings/base.py:163
msgid "Catalan"
msgstr "Catalan"

#: FlOpEDT/settings/base.py:164
msgid "Corsican"
msgstr "Corse"

#: FlOpEDT/settings/base.py:165
msgid "Danish"
msgstr "Espagnol"

#: FlOpEDT/settings/base.py:166
msgid "German"
msgstr "Allemand"

#: FlOpEDT/settings/base.py:167
msgid "Dutch"
msgstr "Néerlandais"

#: FlOpEDT/settings/base.py:168
msgid "Greek"
msgstr "Grec"

#: FlOpEDT/settings/base.py:169
msgid "Italian"
msgstr "Italien"

#: FlOpEDT/settings/base.py:170
msgid "Latin"
msgstr "Latin"

#: FlOpEDT/settings/base.py:171
msgid "Norwegian"
msgstr "Norvégien"

#: FlOpEDT/settings/base.py:172
msgid "Portuguese"
msgstr "Portugais"

#: FlOpEDT/settings/base.py:173
msgid "Swedish"
msgstr "Suédois"

#: FlOpEDT/settings/base.py:174
msgid "Chinese"
msgstr "Chinois"

#: FlOpEDT/settings/base.py:175
msgid "Smurf"
msgstr "Schtroumpf"

#: base/forms.py:33
msgid "Your e-mail address :"
msgstr "Votre adresse mail :"

#: base/forms.py:36
msgid "Recipient :"
msgstr "Destinataire :"

#: base/forms.py:38
msgid "Pseudo : 4 characters max"
msgstr "Pseudo : 4 caractères max"

#: base/forms.py:40
msgid "Subject of the e-mail :"
msgstr "Sujet du mail"

#: base/forms.py:42
msgid "100 characters max"
msgstr "100 caractères max"

#: base/forms.py:43
msgid "Body of the message :"
msgstr "Corps du message :"

#: base/forms.py:50
msgid "This person doesn't exist."
msgstr "Cette personne n'existe pas."

#: base/forms.py:58
msgid "Ideally"
msgstr "Idéalement"

#: base/forms.py:64 base/templates/base/show-stype.html:134
msgid "Maximum"
msgstr "Maximum"

#: base/models.py:82
msgid "Basic group?"
msgstr "Groupe basique?"

#: base/models.py:149
msgid "Half day"
msgstr "Demie-journée"

#: base/models.py:296
msgid "Abbreviation"
msgstr "Abbréviation"

#: base/models.py:344
msgid "graded?"
msgstr "Noté ?"

#: base/models.py:372
msgid "Suspens?"
msgstr "En suspens?"

#: base/models.py:404
msgid "Graded?"
msgstr "Noté ?"

#: base/models.py:728
msgid "Successives?"
msgstr "Successifs ?"

#: base/models.py:729
msgid "On different days"
msgstr "Des jours différents"

#: base/models.py:749
msgid "Full"
msgstr "Totale"

#: base/models.py:751
msgid "Full generation date"
msgstr "Date de regénération"

#: base/models.py:752
msgid "Stabilized"
msgstr "Stabilisée"

#: base/models.py:754
msgid "Partial generation date"
msgstr "Date de regénération partielle"

#: base/templates/base/contact.html:34 base/templates/base/visio.html:38
msgid "Send"
msgstr "Envoyer"

#: base/templates/base/departments.html:15
msgid "Please choose your department : "
msgstr "Veuillez choisir votre département :"

#: base/templates/base/help.html:34
msgid "Room modification"
msgstr "Modification de salle"

#: base/templates/base/help.html:35
msgid ""
"Right click on the course in question, the available rooms appear. Click on "
"+ to choose a room that is not available for this course. Don't forget to "
"validate any changes."
msgstr ""
"Cliquer droit sur le cours en question, les salles disponibles apparaissent. "
"Cliquer sur + pour choisir une salle non prévue pour ce cours. Ne pas "
"oublier de valider tout changement."

#: base/templates/base/help.html:38
msgid "Create an account"
msgstr "Créer un compte"

#: base/templates/base/help.html:39
msgid "Here"
msgstr "Ici"

#: base/templates/base/help.html:42
msgid "Modification of an account"
msgstr "Modifier un compte"

#: base/templates/base/help.html:43
msgid "Click on his nickname (his initials) at the top right of the page."
msgstr "Cliquer sur son pseudo (ses initiales) en haut à droite de la page."

#: base/templates/base/help.html:45
msgid "Change password"
msgstr "Mot de passe perdu ?"

#: base/templates/base/help.html:46 base/templates/base/help.html:49
msgid "Click on"
msgstr "Click sur"

#: base/templates/base/help.html:46 base/templates/base/help.html:49
msgid "this link"
msgstr "ce lien"

#: base/templates/base/help.html:46
msgid "to change your password."
msgstr "pour changer votre mot de passe."

#: base/templates/base/help.html:48
msgid "Password reminder"
msgstr "Rappel du mot de passe"

#: base/templates/base/help.html:49
msgid "for a password reminder, or via 'Login' at the top of the page."
msgstr "pour un rappel de mot de passe, ou via 'Se connecter' en haut de page."

#: base/templates/base/help.html:51
msgid "Direct link to a particular group"
msgstr "Lien direct vers l'EDT d'un groupe spécifique"

#: base/templates/base/help.html:52
msgid "Add \"?promo=INFO1&gp=1A\" at the end of the url, for example "
msgstr "Ajouter \"?promo=INFO1&gp=1A\" à la fin de l'url, par exemple "

#: base/templates/base/help.html:54
msgid "How availability entry work"
msgstr "Fonctionnement de la saisie des disponibilités"

#: base/templates/base/help.html:55
msgid ""
"When generating the schedule for a given week, if no availability is entered "
"for that week, the typical week defines the availability."
msgstr ""
" Au moment de la génération de l'emploi du temps d'une semaine donnée, si "
"aucune disponibilité n'est renseignée pour cette semaine, la semaine type "
"définit les disponibilités."

#: base/templates/base/help.html:57
msgid "Some Tutorial Videos"
msgstr "Quelques Tutos Vidéos"

#: base/templates/base/module_description.html:35
msgid "Modification of the module description."
msgstr "Modification de la description du module"

#: base/templates/base/module_description.html:41
msgid "Modify"
msgstr "Modifier"

#: base/templates/base/notification.html:29
msgid "I wish to be notified of changes for the"
msgstr "Je souhaite être notifié⋅e des modifications pour les"

#: base/templates/base/notification.html:32
msgid "next weeks."
msgstr "prochaines semaines."

#: base/templates/base/notification.html:33
msgid "(0 if no notification, 1 for the next 7 days, etc.)"
msgstr "(0 si aucune notification, 1 pour les 7 prochains jours, etc.)"

#: base/templates/base/notification.html:36
msgid "Save"
msgstr "Enregistrer"

#: base/templates/base/room_preference.html:14
msgid "Favorite room"
msgstr "Préférences de salles"

#: base/templates/base/room_preference.html:15
msgid "(Ranking your favorite rooms, which may contain ties.)"
msgstr "(Classement de vos salles préférées, pouvant contenir des égalités.)"

#: base/templates/base/show-decale.html:79
msgid "When ?"
msgstr "Quand ?"

#: base/templates/base/show-decale.html:81
msgid "Cancelled class"
msgstr "Cours annulé"

#: base/templates/base/show-decale.html:82
msgid "Pending classes"
msgstr "Cours en attente"

#: base/templates/base/show-decale.html:83
msgid "Course scheduled on week"
msgstr "Cours placé en semaine"

#: base/templates/base/show-decale.html:89
msgid "Why ?"
msgstr "Pourquoi ?"

#: base/templates/base/show-decale.html:90
msgid "(Please fill in the date information)"
msgstr "(Merci de remplir les informations sur la date)"

#: base/templates/base/show-decale.html:98
msgid " Proposals"
msgstr "Propositions"

#: base/templates/base/show-decale.html:104
msgid " How ?"
msgstr "Comment ?"

#: base/templates/base/show-edt.html:49
msgid "Check election"
msgstr "Vérifier élection"

#: base/templates/base/show-edt.html:53
msgid "Elect this copy"
msgstr "Elire cette copie"

#: base/templates/base/show-edt.html:57
msgid "Duplicate this copy"
msgstr "Dupliquer cette copie"

#: base/templates/base/show-edt.html:61
msgid "Delete this copy"
msgstr "Supprimer cette copie"

#: base/templates/base/show-edt.html:65
msgid "Delete all copies except the 0"
msgstr "Supprimer toutes les copies sauf la 0"

#: base/templates/base/show-edt.html:68
msgid "Reassign rooms"
msgstr "Réassigner les salles"

#: base/templates/base/show-stype.html:95
msgid "Weeks"
msgstr "Semaines particulières"

#: base/templates/base/show-stype.html:96
msgid "Set default week"
msgstr "Appliquer la configuration ci-contre"

#: base/templates/base/show-stype.html:97
msgid "from week"
msgstr "de la semaine"

#: base/templates/base/show-stype.html:98
#: base/templates/base/show-stype.html:101
msgid "year"
msgstr "année"

#: base/templates/base/show-stype.html:100
msgid "until week"
msgstr "à la semaine"

#: base/templates/base/show-stype.html:103
msgid "button_apply"
msgstr "Appliquer"

#: base/templates/base/show-stype.html:108
msgid "Default week"
msgstr "Semaine type"

#: base/templates/base/show-stype.html:110
msgid "Update my default week"
msgstr "Redéfinir ma semaine type"

#: base/templates/base/show-stype.html:112
#: base/templates/base/show-stype.html:137
msgid "button_save"
msgstr "Sauvegarder"

#: base/templates/base/show-stype.html:121
msgid "My videoconf links"
msgstr "Mes visios"

#: base/templates/base/show-stype.html:122
msgid "Create a link"
msgstr "Créer un lien visio"

#: base/templates/base/show-stype.html:124
#: base/templates/base/show-stype.html:147
msgid "button_change"
msgstr "Modifier"

#: base/templates/base/show-stype.html:127
msgid "My ideal day"
msgstr "Ma journée idéale"

#: base/templates/base/show-stype.html:130
msgid ""
"This is the total number of teaching hours I would like to give every day:"
msgstr ""
"Chaque jour, voici le nombre d'heures de cours que je souhaite donner :"

#: base/templates/base/show-stype.html:132
msgid "Preferred"
msgstr "Idéalement"

#: base/templates/base/show-stype.html:144
msgid "My favorite rooms"
msgstr "Mes préférences de salles"

#: base/templates/base/show-stype.html:145
msgid "Select my favorite rooms"
msgstr "Choisir mes salles préférées"

#: base/templates/base/show-stype.html:170
msgid "Teachers"
msgstr "Enseignant·e⋅s"

#: base/templates/base/show-stype.html:171
#: base/templates/base/show-stype.html:191
msgid "Transformation in : "
msgstr "Tranformation en : "

#: base/templates/base/show-stype.html:177
#: base/templates/base/show-stype.html:183
msgid "Types of classes"
msgstr "Type de cours"

#: base/templates/base/show-stype.html:190 ics/templates/ics/index.html:68
msgid "Rooms"
msgstr "Salles"

#: base/templates/base/statistics.html:105
msgid "Number of days of innocuousness per room"
msgstr "Nombre de jours d'innocupation par salle"

#: base/templates/base/statistics.html:106
msgid "Number of classes given per teacher"
msgstr "Nombre de cours donnés par professeur"

#: base/templates/base/visio.html:31
msgid "Create or modify a link"
msgstr "Créer ou modifier un lien"

#: base/views.py:1700
msgid "Not authorized. New link then."
msgstr "Non autorisé. Création d'un nouveau lien."

#: base/views.py:1703
msgid "Unknown link. New link then."
msgstr "Lien inconnu. Création d'un nouveau lien."

#: base/views.py:1715
msgid "Created"
msgstr "Créé"

#: base/views.py:1715
msgid "Modified"
msgstr "Modifié"

#: base/views.py:1718
msgid "Tutor does not exist"
msgstr "L'utilisateur n'existe pas."

#: base/weeks.py:34
msgid "Mon."
msgstr "Lun."

#: base/weeks.py:35
msgid "Tue."
msgstr "Mar."

#: base/weeks.py:36
msgid "Wed."
msgstr "Mer."

#: base/weeks.py:37
msgid "Thu."
msgstr "Jeu."

#: base/weeks.py:38
msgid "Fri."
msgstr "Ven."

#: base/weeks.py:39
msgid "Sat."
msgstr "Sam."

#: base/weeks.py:40
msgid "Sun."
msgstr "Dim."

#: configuration/templates/configuration/configuration.html:11
msgid ""
"Step 1 - Database configuration (teachers, groups, rooms, course types, etc.)"
msgstr ""
"Étape 1 - Configuration de la base de données (profs, groupes, salles, types "
"de cours, etc.)"

#: configuration/templates/configuration/configuration.html:12
msgid "Option A: Using a spreadsheet file"
msgstr "Option A : En utilisant un fichier tableur"

#: configuration/templates/configuration/configuration.html:14
msgid "Configuration file canvas"
msgstr "Canevas du fichier de configuration"

#: configuration/templates/configuration/configuration.html:15
#: configuration/templates/configuration/configuration.html:50
msgid "Download and then complete the file below."
msgstr "Téléchargez puis remplissez le fichier ci-dessous."

#: configuration/templates/configuration/configuration.html:16
msgid "Click to download the configuration file"
msgstr "Cliquez pour télécharger le fichier de configuration"

#: configuration/templates/configuration/configuration.html:17
#: configuration/templates/configuration/configuration.html:58
msgid "Download"
msgstr "Télécharger"

#: configuration/templates/configuration/configuration.html:21
msgid "Configuration file completed"
msgstr "Fichier de configuration complété"

#: configuration/templates/configuration/configuration.html:22
msgid "Upload the completed configuration file."
msgstr "Téléversez le fichier de configuration rempli."

#: configuration/templates/configuration/configuration.html:23
#: configuration/templates/configuration/configuration.html:65
msgid "Only xlsx format is accepted."
msgstr "Seul le format xlsx est accepté."

#: configuration/templates/configuration/configuration.html:27
#: ics/templates/ics/index.html:84
msgid "Department"
msgstr "Département"

#: configuration/templates/configuration/configuration.html:28
#: people/templates/people/login.html:86
msgid "Create"
msgstr "Créer"

#: configuration/templates/configuration/configuration.html:29
msgid "Edit"
msgstr "Modifier"

#: configuration/templates/configuration/configuration.html:32
#: configuration/templates/configuration/configuration.html:76
#: templates/base.html:94
msgid "Import"
msgstr "Importer"

#: configuration/templates/configuration/configuration.html:37
#: configuration/templates/configuration/configuration.html:79
msgid "Verdict :"
msgstr "Verdict :"

#: configuration/templates/configuration/configuration.html:41
msgid "Option B: With Flopeditor"
msgstr "Option B : Avec Flopeditor"

#: configuration/templates/configuration/configuration.html:43
msgid "Click here"
msgstr "Cliquer ici"

#: configuration/templates/configuration/configuration.html:43
msgid " to go to Flopeditor."
msgstr "pour vous rendre sur Flopeditor."

#: configuration/templates/configuration/configuration.html:44
msgid "If you need help, you can consult the Flopeditor documentation on "
msgstr ""
"Si vous avez besoin d'aide, vous pouvez consulter la documentation de "
"Flopeditor sur "

#: configuration/templates/configuration/configuration.html:44
msgid "this page."
msgstr "cette page."

#: configuration/templates/configuration/configuration.html:47
msgid ""
"Step 2 - Plan the lessons (which lesson in which week, which teacher gives "
"which lesson)"
msgstr ""
"Étape 2 - Planifier les cours (quel cours dans quelle semaine, quel prof "
"donne quel cours)"

#: configuration/templates/configuration/configuration.html:49
msgid "Template for the planning file from the configuration file"
msgstr ""
"Canevas du fichier de planification à partir du fichier de configuration"

#: configuration/templates/configuration/configuration.html:51
msgid "Click to download the planning file outline"
msgstr "Cliquez pour télécharger le canevas du fichier de planification"

#: configuration/templates/configuration/configuration.html:63
msgid "Completed planning file"
msgstr "Fichier de planification des cours complété"

#: configuration/templates/configuration/configuration.html:64
msgid "Upload the completed planning file."
msgstr "Téléversez le fichier de planification rempli."

#: configuration/templates/configuration/configuration.html:75
msgid "Stabilization ?"
msgstr "Stabilisation ?"

#: ics/templates/ics/index.html:35
msgid "iCal Subscription"
msgstr "Abonnement iCal"

#: ics/templates/ics/index.html:36
msgid ""
"Copy the corresponding link of the calendar you would like to add to your "
"calendar manager."
msgstr ""
"Copiez le lien du calendrier qui vous intéresse, et importez-le dans votre "
"gestionnaire d'agenda préféré."

#: ics/templates/ics/index.html:38
msgid "Groups"
msgstr "Groupes"

#: ics/templates/ics/index.html:41
msgid "Training program"
msgstr "Promotion"

#: ics/templates/ics/index.html:41
msgid "Group"
msgstr "Groupe"

#: ics/templates/ics/index.html:46 ics/templates/ics/index.html:62
#: ics/templates/ics/index.html:75 ics/templates/ics/index.html:88
msgid "Copy this link"
msgstr "Copier ce lien"

#: ics/templates/ics/index.html:52
msgid "Tutors"
msgstr "Enseignant⋅e⋅s"

#: ics/templates/ics/index.html:55
msgid "Pseudo"
msgstr "Pseudo"

#: ics/templates/ics/index.html:55
msgid "First name"
msgstr "Prénom "

#: ics/templates/ics/index.html:55
msgid "Last lame"
msgstr "Nom "

#: ics/templates/ics/index.html:55
msgid "Department(s)"
msgstr "Départements"

#: ics/templates/ics/index.html:55 ics/templates/ics/index.html:71
#: ics/templates/ics/index.html:84
msgid "Address"
msgstr "Adresse"

#: ics/templates/ics/index.html:71
msgid "Name"
msgstr "Nom"

#: ics/templates/ics/index.html:81
msgid "Planned generations"
msgstr "Générations prévues"

#: people/templates/people/login.html:55
msgid "Forgot password?"
msgstr "Mot de passe perdu ?"

#: people/templates/people/login.html:58 templates/base.html:108
msgid "Sign in"
msgstr "Se connecter"

#: people/templates/people/login.html:79
msgid "Sign up"
msgstr "Nouveau compte"

#: people/templates/people/login.html:80
msgid "I am"
msgstr "Je suis"

#: people/templates/people/login.html:82
msgid "A student"
msgstr "Étudiant·e"

#: people/templates/people/login.html:83
msgid "A teacher"
msgstr "Enseignant·e permanent·e"

#: people/templates/people/login.html:84
msgid "A temporary teacher"
msgstr "Vacataire"

#: people/templates/people/login.html:85
msgid "A tech or administrative staff"
msgstr "BIATS"

#: people/templates/people/studentPreferencesSelection.html:34
#, fuzzy
#| msgid "Preferences"
msgid "Student Preferences"
msgstr "Préférences"

#: people/templates/people/studentPreferencesSelection.html:43
msgid "Preferences for your group"
msgstr "Préférences pour votre groupe"

#: people/templates/people/studentPreferencesSelection.html:45
msgid "Morning or evening?"
msgstr "Du matin ou du soir ?"

#: people/templates/people/studentPreferencesSelection.html:52
msgid "Busy days VS Free half-days"
msgstr "Journées chargées VS demie-journées libres"

#: people/templates/people/studentPreferencesSelection.html:60
msgid "Holes?"
msgstr "Les trous"

#: people/templates/people/studentPreferencesSelection.html:66
msgid "Eat early or late?"
msgstr "Manger tôt ou tard ?"

#: people/views.py:237
msgid "Not allowed"
msgstr "Non autorisé"

#: people/views.py:243
#, python-brace-format
msgid "No such user as {user}"
msgstr "Aucun utilisateur {user}"

#: quote/forms.py:41
msgid "Quote "
msgstr "Citation "

#: quote/forms.py:42
msgid "Last name "
msgstr "Nom "

#: quote/forms.py:43
msgid "First name "
msgstr "Prénom "

#: quote/forms.py:44
msgid "Nickname "
msgstr "Pseudo "

#: quote/forms.py:45
msgid "Author description "
msgstr "Fonction, description<br/> de l'auteur/autrice "

#: quote/forms.py:46
msgid "Date "
msgstr "Date "

#: quote/forms.py:47
msgid "Header "
msgstr "En-tête "

#: quote/forms.py:48
msgid "Category "
msgstr "Catégorie "

#: solve_board/templates/solve_board/main-board.html:32
#, fuzzy
#| msgid "Schedule_banner"
msgid "Schedule Generation"
msgstr "Consulter"

#: solve_board/templates/solve_board/main-board.html:60
msgid "Constraints"
msgstr "Contraintes"

#: solve_board/templates/solve_board/main-board.html:61
#, fuzzy
#| msgid ""
#| "All words appearing in bolded - with no weight - will be considered "
#| "mandatory, the other ones will considered as preferences."
msgid ""
"All constraints appearing in bolded - with no weight - will be considered "
"mandatory, the other ones will considered as preferences."
msgstr ""
"Les contraintes apparaissant en gras - avec un poids non défini - seront "
"prises en compte de manièreobligatoire, les autres seront utilisées dans la "
"résolution comme des préférences."

#: solve_board/templates/solve_board/main-board.html:66
#, fuzzy
#| msgid "Configuration file canvas"
msgid "Configuration"
msgstr "Canevas du fichier de configuration"

#: solve_board/templates/solve_board/main-board.html:70
#, fuzzy
#| msgid "Weeks"
msgid "Week⋅s"
msgstr "Semaines particulières"

#: solve_board/templates/solve_board/main-board.html:74
#, fuzzy
#| msgid "Training program"
msgid "Training Program"
msgstr "Promotion"

#: solve_board/templates/solve_board/main-board.html:78
#, fuzzy
#| msgid "Stabilization ?"
msgid "Stabilization"
msgstr "Stabilisation ?"

#: solve_board/templates/solve_board/main-board.html:83
msgid "Solver"
msgstr "Solveur"

#: solve_board/templates/solve_board/main-board.html:94
msgid "Maximum time limit for resolution (minuts)"
msgstr "Durée limite de résolution (minutes)"

#: solve_board/templates/solve_board/main-board.html:100
msgid "Unlimited"
msgstr "Illimité"

#: templates/base.html:53
msgid "Schedule_title"
msgstr "Emplois du temps"

#: templates/base.html:66
msgid "<span id=flopGreen >flop</span>!<span id=EDTRed >Scheduler</span>"
msgstr "<span id=flopGreen >flop</span>!<span id=EDTRed >EDT</span>"

#: templates/base.html:82
msgid "Schedule_banner"
msgstr "Consulter"

#: templates/base.html:84
msgid "Move/Cancel"
msgstr "Décaler/Annuler"

#: templates/base.html:86
msgid "Preferences"
msgstr "Préférences"

#: templates/base.html:87
msgid "iCal"
msgstr "iCal"

#: templates/base.html:88
msgid "Help"
msgstr "Aide"

#: templates/base.html:89
msgid "Messages"
msgstr "Contact"

#: templates/base.html:90
msgid "Modules"
msgstr "Modules"

#: templates/base.html:92
msgid "Generate"
msgstr "Générer"

#: templates/base.html:93
msgid "Statistics"
msgstr "Statistiques"

#: templates/base.html:95
msgid "Admin"
msgstr "Admin"

#: templates/base.html:101 templates/base.html:103 templates/base.html:105
msgid "Sign out"
msgstr "Déconnexion"

#: templates/base.html:112
msgid "Signed in as "
msgstr "Connecté·e sous "

#~ msgid "Scheduler"
#~ msgstr "EDT"

#~ msgid "schedule"
#~ msgstr "edt"

#~ msgid "Class placed during the week"
#~ msgstr "Cours placé en semaine"

#, fuzzy
#~ msgid "Room preferences"
#~ msgstr "Préférences"

#~ msgid "Choose my video conference rooms"
#~ msgstr "Mes liens visio"

#~ msgid "If no tutor then training programme."
#~ msgstr "Si pas de prof alors promo."

#~ msgid "If no training programme then tutor."
#~ msgstr "Si pas de promo alors prof."
